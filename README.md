# The emergence of discrimination due to miscategorization

This code is used in the following publication:

- Yasar, Alperen. 2024. "The emergence of discrimination due to miscategorization.” _International Journal of Organization Theory & Behavior_.

You can contact me in case you need help working the code.