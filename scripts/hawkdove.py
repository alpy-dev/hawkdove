import matplotlib.pyplot as plt
import numpy as np
import math
import random
import time
import statistics
# import seaborn as sns

import pandas as pd

# np.random.seed(42)
# random.seed(42)i

NUMBER_AGENTS = 240  # or 120, or 480
TURN_AMOUNT = 100  # short term 500?
RUN_AMOUNT = 100  # this is a lot...

ALPHA = 4  # "a" parameter of Beta distribution. Has to be smaller than or equal to BETA.
BETA = 12  # "b" parameter of Beta distribution

TURN_OVER_PROB = 0.01
RELIABILITY = 10

NOISE = 0
MUTATION_RATE = 1


class Agent:

    def __init__(self, unique_id, categorization, education, sex):

        self.unique_id = unique_id  # Has to be an integer.
        self.categorization = categorization  # 0="coarse", 1="regular" or 2="fine"
        self.education = education  # 1 if "prestigious" education; 0 else.
        self.sex = sex  # 1 if "male"; 0 if female.
        self.age = 0  # Has to be an integer.
        self.struggle_results = np.ones(4)  # How many victories against: Non-education female; education female;
        # non-education male and finally education male.
        self.struggle_counters = np.ones(4)  # How many matches against possible categories.

        if education == 0:
            self.rhp = np.random.beta(ALPHA, BETA)
        else:
            self.rhp = np.random.beta(BETA, ALPHA)

        self.total_payoff = 0
        self.avg_payoff = 0

        self.last_payoff = None
        self.last_action = None
        self.last_enemy_action = None

    def __repr__(self):
        """
        This function allows for calling an agent with her unique ID.
        """
        return "Agent(%d)" % self.unique_id

    def match(self, **kwargs):
        """
        This function allows for finding an agent with her parameters.
        """

        return all(getattr(self, key) == val for (key, val) in kwargs.items())

    def calc_avg_payoff(self):
        if self.age != 0:
            self.avg_payoff = self.total_payoff / self.age
        else:
            pass

    def imitate_if_better(self, opponent_list):
        opponent = random.choice(opponent_list)
        if opponent.avg_payoff > self.avg_payoff:
            self.categorization = opponent.categorization
        else:
            pass

    def imitate_with_probability(self, opponent_list, m=0.1):
        """
        Same as imitate_if_better, except only imitates with a probability.
        :param m: Fixed probability of imitating.
        """
        if random.random() < m:
            opponent = random.choice(opponent_list)
            if opponent.avg_payoff > self.avg_payoff:
                self.categorization = opponent.categorization
            else:
                pass

    def imitative_logit(self, opponent_list, mr=0.01):  # best experienced payoff protocol
        """
        Imitates an opponent with probability proportional to the average payoff difference between agents.
        This is the imitation strategy we use for final simulations.
        """
        mr = mr
        opponent = random.choice(opponent_list)
        if opponent.avg_payoff == self.avg_payoff == 0:
            pass
        else:
            copy_probability = math.exp(opponent.avg_payoff*mr) / \
                               (math.exp(self.avg_payoff*mr) + math.exp(opponent.avg_payoff*mr))

            if random.random() < copy_probability:
                self.categorization = opponent.categorization
            else:
                pass

    def update(self, opponent, match_result):

        """
        Agents updates her memory according to her categorization strategy given an opponent and a match result
        :param opponent: Required because a categorizing agent needs to observe her opponent's sex and education.
        :param match_result: 0 for lose, 1 for win.
        """

        if self.categorization == 0:  # Coarse-categorizer agents update every column.
            self.struggle_counters[:] += 1
            self.struggle_results[:] += match_result
        elif self.categorization == 1:  # Regular-categorizer agents update two columns based on opponent's education.
            self.struggle_counters[opponent.education] += 1
            self.struggle_counters[opponent.education+2] += 1
            self.struggle_results[opponent.education] += match_result
            self.struggle_results[opponent.education+2] += match_result
        else:  # Fine-categorizer agents update only one column, depending on opponent's education and sex.
            self.struggle_counters[2 * opponent.sex + opponent.education] += 1
            self.struggle_results[2 * opponent.sex + opponent.education] += match_result

    def asses_opponent(self, opponent):
        """
        An agents decides her expected struggling power against an opponent based on her opponent's education and sex.
        :return: Returns the agent's expected struggling power, a value between 0 and 1.
        """

        if self.categorization == 0:
            erhp = sum(self.struggle_results) / sum(self.struggle_counters)
        elif self.categorization == 1:
            erhp = (self.struggle_results[opponent.education] + self.struggle_results[opponent.education+2]) / \
                   (self.struggle_counters[opponent.education] + self.struggle_counters[opponent.education+2])
        else:
            erhp = self.struggle_results[2*opponent.sex + opponent.education] / \
                   self.struggle_counters[2*opponent.sex + opponent.education]
        return erhp


class Game:

    def __init__(self, player1, player2, v=10, c=20, zeta=25, noise=0.0):

        self.p1 = player1
        self.p2 = player2
        self.v = v
        self.c = c
        self.zeta = zeta
        self.noise = noise

        self.rhp1 = 1 / (1 + math.exp(-(self.p1.rhp - self.p2.rhp) * zeta))
        self.rhp2 = 1 - self.rhp1

        self.erhp1 = self.p1.asses_opponent(self.p2)
        self.erhp2 = self.p2.asses_opponent(self.p1)
        #
        # if self.erhp1 == 0:
        #     self.erhp1 = 0.001
        #
        # if self.erhp2 == 0:
        #     self.erhp2 = 0.001
        #
        # if self.erhp1 == 1:
        #     self.erhp1 = 0.999
        #
        # if self.erhp2 == 1:
        #     self.erhp2 = 0.999

    def play(self):

        noise1 = random.random()
        noise2 = random.random()

        if noise1 > self.noise:
            if self.c/self.v < (1 - self.erhp1) / self.erhp1:
                strat1 = "hawk"
            else:
                if self.erhp1 > 0.5:
                    strat1 = "hawk"
                else:
                    strat1 = "dove"
        else:
            strat1 = "hawk"

        if noise2 > self.noise:
            if self.c/self.v < (1 - self.erhp2) / self.erhp2:
                strat2 = "hawk"
            else:
                if self.erhp2 > 0.5:
                    strat2 = "hawk"
                else:
                    strat2 = "dove"
        else:
            strat2 = "hawk"

        if strat1 == strat2 == "hawk":
            if random.random() < self.rhp1:
                self.p1.last_payoff = self.v
                self.p2.last_payoff = self.c
                self.p1.last_action = "hawk"
                self.p2.last_action = "hawk"
                self.p1.update(self.p2, 1)
                self.p2.update(self.p1, 0)
            else:
                self.p1.last_payoff = self.c
                self.p2.last_payoff = self.v
                self.p1.last_action = "hawk"
                self.p2.last_action = "hawk"
                self.p1.update(self.p2, 0)
                self.p2.update(self.p1, 1)

        elif strat1 == strat2 == "dove":
            self.p1.last_payoff = self.v/2
            self.p1.last_action = "dove"
            self.p2.last_payoff = self.v/2
            self.p2.last_action = "dove"

        elif strat1 == "hawk" and strat2 == "dove":
            self.p1.last_payoff = self.v
            self.p1.last_action = "hawk"
            self.p2.last_payoff = self.c
            self.p2.last_action = "dove"

        else:
            self.p2.last_payoff = self.v
            self.p2.last_action = "hawk"
            self.p1.last_payoff = self.c
            self.p1.last_action = "dove"

        self.p1.total_payoff += self.p1.last_payoff
        self.p2.total_payoff += self.p2.last_payoff

        self.p1.last_enemy_action = self.p2.last_action
        self.p2.last_enemy_action = self.p1.last_action


class Environment:

    def __init__(self, turn_amount, run_amount, pop_size, turn_over_prob=0.01,
                 reward=10, cost=20, reliability=10, noise=0.01, mutation_rate=0.1):
        self.total_turn = turn_amount
        self.total_run = run_amount
        self.pop_size = pop_size
        self.current_turn = 0
        self.current_run = 0
        self.reward = reward
        self.cost = cost
        self.reliability = reliability
        self.noise = noise
        self.turn_over_prob = turn_over_prob  # This is essentially mutation rate.
        self.mutation_rate = mutation_rate  # This is actually mutation rate...

        self.__agents_list = []

        self.total_avg_payoff = 0

        self.run_df = pd.DataFrame()

    def add_new_agent(self, m=0.1):
        rand = random.random()

        exp_male_nonedu = math.exp(self.avg_male_nonedu_payoff*m)
        exp_male_edu = math.exp(self.avg_male_edu_payoff*m)
        exp_female_nonedu = math.exp(self.avg_female_nonedu_payoff * m)
        exp_female_edu = math.exp(self.avg_female_edu_payoff*m)
        div = exp_male_edu + exp_male_nonedu + exp_female_edu + exp_female_nonedu

        male_nonedu_prob = exp_male_nonedu / div
        female_nonedu_prob = exp_female_nonedu / div
        male_edu_prob = exp_male_edu / div
        female_edu_prob = exp_female_edu / div
        #
        # if male_nonedu_prob == 0:
        #     male_nonedu_prob = 0.01
        # if male_edu_prob == 0:
        #     male_edu_prob = 0.05
        # if female_edu_prob == 0:
        #     female_edu_prob = 0.01
        # if female_nonedu_prob == 0:
        #     female_nonedu_prob = 0.01

        if rand < female_nonedu_prob:
            sex = 0
            education = 0

        elif rand < female_nonedu_prob + female_edu_prob:
            sex = 0
            education = 1

        elif rand < female_nonedu_prob + female_edu_prob + male_nonedu_prob:
            sex = 1
            education = 0

        else:
            sex = 1
            education = 1

        categorization = np.random.randint(3)
        self.__agents_list.append(Agent(self.__agents_list[-1].unique_id+1, categorization, education, sex))

    def add_init_agent(self, n):
        self.__agents_list.append(Agent(n, n % 3, n % 2, 0 if n < self.pop_size / 2 else 1))

    def find_agent(self, **kwargs):
        return next(self.__iter_agent(**kwargs))

    def all_agents(self, **kwargs):
        return list(self.__iter_agent(**kwargs))

    def __iter_agent(self, **kwargs):
        return (agent for agent in self.__agents_list if agent.match(**kwargs))

    def avg_payoffs(self, agent_list, **kwargs):
        self.total_avg_payoff = 0
        for agent in agent_list:
            agent.calc_avg_payoff()
            self.total_avg_payoff += agent.avg_payoff
        if len(agent_list) > 0:
            list_avg_payoff = self.total_avg_payoff / len(agent_list)
        else:
            list_avg_payoff = 0
        return list_avg_payoff

    def avg_fighting(self, agent_list, **kwargs):
        total_avg_pressure = 0
        total_avg_aggro = 0
        for agent in agent_list:
            if agent.last_enemy_action == "hawk":
                total_avg_pressure += 1
            if agent.last_action == "hawk":
                total_avg_aggro += 1

        if len(agent_list) > 0:
            list_avg_pressure = total_avg_pressure / len(agent_list)
            list_avg_aggro = total_avg_aggro / len(agent_list)
        else:
            list_avg_pressure = 0
            list_avg_aggro = 0
        return [list_avg_pressure, list_avg_aggro]

    def calc_avg_payoffs(self):

        self.female_nonedu_agents = self.all_agents(education=0, sex=0)
        self.female_edu_agents = self.all_agents(education=1, sex=0)
        self.male_nonedu_agents = self.all_agents(education=0, sex=1)
        self.male_edu_agents = self.all_agents(education=1, sex=1)

        self.avg_female_nonedu_payoff = self.avg_payoffs(self.female_nonedu_agents)
        self.avg_female_edu_payoff = self.avg_payoffs(self.female_edu_agents)
        self.avg_male_nonedu_payoff = self.avg_payoffs(self.male_nonedu_agents)
        self.avg_male_edu_payoff = self.avg_payoffs(self.male_edu_agents)

    def calc_distribution(self):

        self.cfn_agents = self.all_agents(categorization=0, sex=0, education=0)
        self.cfe_agents = self.all_agents(categorization=0, sex=0, education=1)
        self.cmn_agents = self.all_agents(categorization=0, sex=1, education=0)
        self.cme_agents = self.all_agents(categorization=0, sex=1, education=1)
        self.rfn_agents = self.all_agents(categorization=1, sex=0, education=0)
        self.rfe_agents = self.all_agents(categorization=1, sex=0, education=1)
        self.rmn_agents = self.all_agents(categorization=1, sex=1, education=0)
        self.rme_agents = self.all_agents(categorization=1, sex=1, education=1)
        self.sfn_agents = self.all_agents(categorization=2, sex=0, education=0)
        self.sfe_agents = self.all_agents(categorization=2, sex=0, education=1)
        self.smn_agents = self.all_agents(categorization=2, sex=1, education=0)
        self.sme_agents = self.all_agents(categorization=2, sex=1, education=1)
        cfn = len(self.cfn_agents)
        cfe = len(self.cfe_agents)
        cmn = len(self.cmn_agents)
        cme = len(self.cme_agents)
        rfn = len(self.rfn_agents)
        rfe = len(self.rfe_agents)
        rmn = len(self.rmn_agents)
        rme = len(self.rme_agents)
        sfn = len(self.sfn_agents)
        sfe = len(self.sfe_agents)
        smn = len(self.smn_agents)
        sme = len(self.sme_agents)

        distribution = [cfn, cfe, cmn, cme, rfn, rfe, rmn, rme, sfn, sfe, smn, sme]
        return distribution

    def shuffle_agents(self):
        shuffled_list = random.sample(self.__agents_list, len(self.__agents_list))
        return shuffled_list

    def match_robin(self, agents_list):
        for n in range(int(len(agents_list)/2)):
            p1 = agents_list[n]
            p2 = agents_list[n+int(len(agents_list)/2)]
            new_game = Game(p1, p2, v=self.reward, c=self.cost, zeta=self.reliability, noise=self.noise)
            new_game.play()

    def agents_imitate(self, method=0):
        if method == 0:
            for n in self.__agents_list:
                n.imitative_logit(self.__agents_list, self.mutation_rate)

        elif method == 1:
            for n in self.__agents_list:
                n.imitate_with_probability(self.__agents_list, self.mutation_rate)

        else:
            for n in self.__agents_list:
                n.imitate_if_better(self.__agents_list)

    def turn_over_agents(self, turn_over_prob=0.01):
        agents_fired = random.sample(self.__agents_list, int(int(len(self.__agents_list))*turn_over_prob))
        self.__agents_list = [agent for agent in self.__agents_list if agent not in agents_fired]
        for agent in range(len(agents_fired)):
            self.add_new_agent()

    def diff_genders(self, dl):

        females = dl[0] + dl[1] + dl[4] + dl[5] + dl[8] + dl[9]
        males = dl[2] + dl[3] + dl[6] + dl[7] + dl[10] + dl[11]
        self.female_press_aggro = self.avg_fighting(self.female_nonedu_agents + self.female_edu_agents)
        self.male_press_aggro = self.avg_fighting(self.male_nonedu_agents + self.male_edu_agents)
        self.sex_aggro_difference = abs(self.male_press_aggro[1] - self.female_press_aggro[1])
        self.sex_pressure_difference = abs(self.male_press_aggro[0] - self.female_press_aggro[0])

        self.sex_pop_diff = abs(females-males)

    def diff_categories(self, dl):

        self.coarse_agents = self.cfn_agents + self.cmn_agents + self.cfe_agents + self.cme_agents
        self.regular_agents = self.rfn_agents + self.rmn_agents + self.rfe_agents + self.rme_agents
        self.fine_agents = self.sfn_agents + self.smn_agents + self.sfe_agents + self.sme_agents

        self.coarse_aggro = self.avg_fighting(self.coarse_agents)[1]
        self.regular_aggro = self.avg_fighting(self.regular_agents)[1]
        self.fine_aggro = self.avg_fighting(self.fine_agents)[1]

    def one_turn(self):

        shuffled_list = self.shuffle_agents()
        self.match_robin(shuffled_list)
        self.agents_imitate(0)  # Imitative logit algoritm if 0;
        # probabilistic imitation elif 1; imitate if better else.

        self.calc_avg_payoffs()
        self.turn_over_agents(self.turn_over_prob)

        distribution = self.calc_distribution()
        self.diff_genders(distribution)
        self.diff_categories(distribution)

        list_to_df = [[self.current_run], [self.current_turn], distribution, [self.coarse_aggro], [self.regular_aggro],
                      [self.fine_aggro], [self.sex_pop_diff], self.female_press_aggro, self.male_press_aggro,
                      [self.sex_aggro_difference], [self.sex_pressure_difference]]
        self.run_df.loc[len(self.run_df)] = [item for sublist in list_to_df for item in sublist]
        for agent in self.__agents_list:
            agent.age += 1
        self.current_turn += 1

    def one_run(self):

        self.current_turn = 0
        self.__agents_list = []

        self.total_avg_payoff = 0

        self.run_df = pd.DataFrame(columns=["Run", "Turn", "cfn", "cfe", "cmn", "cme", "rfn", "rfe", "rmn", "rme",
                                            "sfn", "sfe", "smn", "sme", "coarse_aggro", "regular_aggro", "fine_aggro",
                                            "sex_pop_diff", "female_pressure", "female_aggro", "male_pressure",
                                            "male_aggro", "sex_aggro_diff", "sex_press_diff"])

        for n in range(self.pop_size):
            self.add_init_agent(n)

        for i in range(self.total_turn):
            self.one_turn()

        return self.run_df

    def one_parameter_combination(self):

        self.current_run = 0
        self.all_run_dataframes = []

        for i in range(self.total_run):

            new_run_df = self.one_run()
            self.all_run_dataframes.append(new_run_df)
            self.current_run += 1
            print(f"Simulating run {self.current_run} for cost {self.cost}")

            # plt.plot(new_run_df['sex_pop_diff'])
            # plt.show()
            # plt.plot(new_run_df['cfn'] + new_run_df['rfn'] + new_run_df['sfn'], label="fn")
            # plt.plot(new_run_df['cfe'] + new_run_df['rfe'] + new_run_df['sfe'], label="fe")
            # plt.plot(new_run_df['cmn'] + new_run_df['rmn'] + new_run_df['smn'], label="mn")
            # plt.plot(new_run_df['cme'] + new_run_df['rme'] + new_run_df['sme'], label="me")
            # plt.legend()
            # plt.show()

        averages_df = pd.concat(self.all_run_dataframes).groupby(level=0).mean()
        averages_df['coarses'] = averages_df['cfn'] + averages_df['cfe'] + averages_df['cmn'] + averages_df['cme']
        averages_df['regulars'] = averages_df['rfn'] + averages_df['rfe'] + averages_df['rmn'] + averages_df['rme']
        averages_df['fines'] = averages_df['sfn'] + averages_df['sfe'] + averages_df['smn'] + averages_df['sme']
        averages_df['coarse_nonedu'] = averages_df['cfn'] + averages_df['cmn']
        averages_df['coarse_edu'] = averages_df['cfe'] + averages_df['cme']
        averages_df['regular_nonedu'] = averages_df['rfn'] + averages_df['rmn']
        averages_df['regular_edu'] = averages_df['rfe'] + averages_df['rme']
        averages_df['fine_nonedu'] = averages_df['sfn'] + averages_df['smn']
        averages_df['fine_edu'] = averages_df['sfe'] + averages_df['sme']
        self.plot_average_categories(averages_df)
        self.plot_gender_segregation(averages_df)
        return averages_df

    def full_sim(self):
        t0 = time.time()
        models = [20, 40, 60, 80, 100]

        aggro_means = []
        aggro_stds = []
        pressure_means = []
        pressure_stds = []

        coarse_means = []
        regular_means = []
        fine_means = []

        coarse_stds = []
        regular_stds = []
        fine_stds =[]

        coarse_nonedu_means = []
        coarse_edu_means = []
        regular_nonedu_means = []
        regular_edu_means = []
        fine_nonedu_means = []
        fine_edu_means = []

        coarse_nonedu_stds = []
        coarse_edu_stds = []
        regular_nonedu_stds = []
        regular_edu_stds = []
        fine_nonedu_stds = []
        fine_edu_stds = []

        coarse_aggro_means = []
        regular_aggro_means = []
        fine_aggro_means = []

        coarse_aggro_stds =[]
        regular_aggro_stds = []
        fine_aggro_stds = []

        for model in models:
            self.cost = model
            averages_df = self.one_parameter_combination()
            aggro_means.append(statistics.mean(averages_df['sex_aggro_diff']))
            aggro_stds.append(statistics.stdev(averages_df['sex_aggro_diff']))
            pressure_means.append(statistics.mean(averages_df['sex_press_diff']))
            pressure_stds.append(statistics.stdev(averages_df['sex_press_diff']))

            coarse_nonedu_means.append(statistics.mean(averages_df['coarse_nonedu']))
            coarse_edu_means.append(statistics.mean(averages_df['coarse_edu']))
            regular_nonedu_means.append(statistics.mean(averages_df['regular_nonedu']))
            regular_edu_means.append(statistics.mean(averages_df['regular_edu']))
            fine_nonedu_means.append(statistics.mean(averages_df['fine_nonedu']))
            fine_edu_means.append(statistics.mean(averages_df['fine_edu']))

            coarse_nonedu_stds.append(statistics.stdev(averages_df['coarse_nonedu']))
            coarse_edu_stds.append(statistics.stdev(averages_df['coarse_edu']))
            regular_nonedu_stds.append(statistics.stdev(averages_df['regular_nonedu']))
            regular_edu_stds.append(statistics.stdev(averages_df['regular_edu']))
            fine_nonedu_stds.append(statistics.stdev(averages_df['fine_nonedu']))
            fine_edu_stds.append(statistics.stdev(averages_df['fine_edu']))

            coarse_means.append(statistics.mean(averages_df['coarses']))
            regular_means.append(statistics.mean(averages_df['regulars']))
            fine_means.append(statistics.mean(averages_df['fines']))

            coarse_stds.append(statistics.stdev(averages_df['coarses']))
            regular_stds.append(statistics.stdev(averages_df['regulars']))
            fine_stds.append(statistics.stdev(averages_df['fines']))

            coarse_aggro_means.append(statistics.mean(averages_df['coarse_aggro']))
            regular_aggro_means.append(statistics.mean(averages_df['regular_aggro']))
            fine_aggro_means.append(statistics.mean(averages_df['fine_aggro']))

            coarse_aggro_stds.append(statistics.stdev(averages_df['coarse_aggro']))
            regular_aggro_stds.append(statistics.stdev(averages_df['regular_aggro']))
            fine_aggro_stds.append(statistics.stdev(averages_df['fine_aggro']))

        for i in range(len(models)):
            # coarses = coarse_means[i]
            # regulars = regular_means[i]
            # fines = fine_means[i]
            #
            non_edu_pop = coarse_nonedu_means[i] + regular_nonedu_means[i] + fine_nonedu_means[i]
            edu_pop = coarse_edu_means[i] + regular_edu_means[i] + fine_edu_means[i]

            coarses = coarse_means [i] * 100 / self.pop_size
            regulars = regular_means [i] * 100 / self.pop_size
            fines = fine_means [i] * 100 / self.pop_size

            coarses_std = coarse_stds [i] * 100 / self.pop_size
            regulars_std = regular_stds [i] * 100 / self.pop_size
            fines_std = fine_stds [i] * 100 / self.pop_size

            coarse_nonedus = coarse_nonedu_means[i] * 100 / non_edu_pop
            coarse_edus = coarse_edu_means[i] * 100 / edu_pop
            regular_nonedus = regular_nonedu_means[i] * 100 / non_edu_pop
            regular_edus = regular_edu_means[i] * 100 / edu_pop
            fine_nonedus = fine_nonedu_means[i] * 100 / non_edu_pop
            fine_edus = fine_edu_means[i] * 100 / edu_pop

            coarse_nonedus_std = coarse_nonedu_stds[i] * 100 / non_edu_pop
            coarse_edus_std = coarse_edu_stds[i] * 100 / edu_pop
            regular_nonedus_std = regular_nonedu_stds[i] * 100 / non_edu_pop
            regular_edus_std = regular_edu_stds[i] * 100 / edu_pop
            fine_nonedus_std = fine_nonedu_stds[i] * 100 / non_edu_pop
            fine_edus_std = fine_edu_stds[i] * 100 / edu_pop

            categorizations = ["Coarse", "Regular", "Fine"]
            nonedu_pop = [coarse_nonedus, regular_nonedus, fine_nonedus]
            edu_pop = [coarse_edus, regular_edus, fine_edus]
            non_edu_stds = [coarse_nonedus_std, regular_nonedus_std, fine_nonedus_std]
            edu_stds = [coarse_edus_std, regular_edus_std, fine_edus_std]

            total_pop = [coarses, regulars, fines]
            total_stds = [coarses_std, regulars_std, fines_std]

            # fig = plt.figure()
            # ax = fig.add_axes([0, 0, 1, 1])
            # categorizations = ["Coarse", "Regular", "Fine"]
            # populations = [coarses, regulars, fines]

            colors = ['#1f77b4', '#ff7f0e', '#2ca02c']
            plt.bar(categorizations, total_pop, yerr=total_stds, fill=False, edgecolor='black', lw=1, width=0.4, capsize=6)
            plt.ylabel('Percentage of agents')
            plt.title(f'C/V = {models[i]/10}')
            plt.show()
            #
            # plt.bar(categorizations, edu_pop, yerr=edu_stds, fill=False, edgecolor='black', lw=1, width=0.4, capsize=6)
            # plt.ylabel('Percentage of agents with prestigious education')
            # plt.title(f'C/V = {models[i]/10}')
            # plt.show()

            # ax.set_ylabel('Average population')
            # # ax.set_title('Categorization by educations')
            # ax.set_xticks(x, educations)
            # ax.legend()
            # fig.tight_layout()
            # fig.savefig(f'{i}.png', dpi=1200)
            # plt.show()

        plt.errorbar(models, aggro_means, aggro_stds, color='black', ecolor='black', fmt='o', label='errorbar', linewidth=2, capsize=6)
        plt.xlabel('Cost of fighting')
        plt.ylabel('The difference in aggressiveness')
        plt.show()

        plt.errorbar(models, pressure_means, pressure_stds, color='black', ecolor='black', fmt='o', label='errorbar', linewidth=2, capsize=6)
        plt.xlabel('Cost of fighting')
        plt.ylabel('The difference in being subject to aggressiveness')
        plt.show()

        costs = ["Low", "High"]
        x=np.arange(2)
        width = 0.2
        plt.bar(x-0.2, coarse_aggro_means, width=width, yerr=coarse_aggro_stds, hatch='/', fill=False, edgecolor='black', lw=1, capsize=6, label="Coarse")
        plt.bar(x, regular_aggro_means, width=width, yerr=regular_aggro_stds, hatch='+', fill=False, edgecolor='black', lw=1, capsize=6, label="Regular")
        plt.bar(x+0.2, fine_aggro_means, width=width, yerr=fine_aggro_stds, hatch='.', fill=False, edgecolor='black', lw=1, capsize=6, label="Fine")
        plt.xticks(x, costs)
        plt.xlabel('Cost of fighting')
        plt.ylabel('Aggressiveness')
        plt.legend()
        # plt.title(f'C/V = {models[i] / 10}')
        plt.show()

        # plt.errorbar(costs, coarse_aggro_means, coarse_aggro_stds, fmt='o', label='errorbar', linewidth=2, capsize=6)
        # plt.errorbar(costs, regular_aggro_means, regular_aggro_stds, fmt='o', label='errorbar', linewidth=2, capsize=6)
        # plt.errorbar(costs, fine_aggro_means, fine_aggro_stds, fmt='o', label='errorbar', linewidth=2, capsize=6)
        # plt.xlabel('Cost of fighting')
        # plt.ylabel('Aggressiveness by categorization')
        # plt.show()

        t1 = time.time()
        total = t1-t0
        print(total/60)

    def plot_average_categories(self, df):

        # std = self.all_run_dataframes.groupby('category')['number'].std()
        coarse_columns = ['cfn', 'cfe', 'cmn', 'cme']
        regular_columns = ['rfn', 'rfe', 'rmn', 'rme']
        sexist_columns = ['sfn', 'sfe', 'smn', 'sme']

        df['Coarse_Averages'] = df[coarse_columns].sum(axis=1)
        df['Regular_Averages'] = df[regular_columns].sum(axis=1)
        df['Sexist_Averages'] = df[sexist_columns].sum(axis=1)


        plt.figure()
        plt.plot(df['Coarse_Averages'], linestyle='--', marker='o', markevery=250, markersize=6, color='black', lw=0.5, label="Coarse")
        plt.plot(df['Regular_Averages'], linestyle='-.', marker='s', markevery=250, markersize=6, color='black', lw=0.5, label="Regular")
        plt.plot(df['Sexist_Averages'], linestyle='dotted', marker='^', markevery=250, markersize=6, color='black', lw=0.5, label="Fine")
        plt.ylabel('Average population')
        plt.title(f'C/V = {self.cost/10}')
        plt.legend()
        plt.show()
        #
        # plt.figure()
        # plt.plot(df['coarse_aggro'], linestyle='--', color='black', lw=1, label="Coarse")
        # plt.plot(df['regular_aggro'], linestyle='-.', color='black', lw=1, label="Regular")
        # plt.plot(df['fine_aggro'], linestyle='-', color='black', lw=1, label="Fine")
        # plt.ylabel('Average aggressiveness')
        # plt.title(f'C/V = {self.cost/10}')
        # plt.legend()
        # plt.show()

    def plot_gender_segregation(self, df):
        df = pd.concat(self.all_run_dataframes).groupby(level=0).mean()

        # std = self.all_run_dataframes.groupby('category')['number'].std()
        female_columns = ['cfn', 'cfe', 'rfn', 'rfe', 'sfn', 'sfe']
        male_columns = ['cmn', 'cme', 'rmn', 'rme', 'smn', 'sme']

        df['Female_Pop'] = df[female_columns].sum(axis=1)
        df['Male_Pop'] = df[male_columns].sum(axis=1)


def sigmoid(sep=200, zeta=10):

    x = np.linspace(-1, 1, sep)

    z = 1 / (1 + np.exp(-x * zeta))

    plt.plot(x, z)
    plt.xlabel("Power Difference")
    plt.ylabel("Probability of winning when zeta is 10")

    plt.show()


c20 = Environment(turn_amount=TURN_AMOUNT, run_amount=RUN_AMOUNT, pop_size=NUMBER_AGENTS,
                  turn_over_prob=TURN_OVER_PROB,
                  reward=10, cost=20,
                  reliability=RELIABILITY, noise=NOISE, mutation_rate=MUTATION_RATE)
c20.full_sim()
#
# sigmoid()