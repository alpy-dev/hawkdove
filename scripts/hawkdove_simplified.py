# import matplotlib.pyplot as plt
# import numpy as np
# import math
# import random
# import time
# from scipy.stats import poisson
#
# import pandas as pd
#
# NUMBER_AGENTS = 12  # or 120, or 480
# TOTAL_TURN = 5000  # short term 500?
# TOTAL_RUN = 500  # this is a lot...
#
# ALPHA = 4  # "a" parameter of Beta distribution. Has to be smaller than or equal to BETA.
# BETA = 12  # "b" parameter of Beta distribution
#
#
# class Agent:
#
#     def __init__(self, unique_id, categorization, sex, memory_size=20):
#
#         self.unique_id = unique_id  # Integer
#         self.categorization = categorization  # 0="regular", 1="sexist"
#         self.sex = sex  # 1 if "female" (0 else).
#         self.age = 0
#         # self.max_age = poisson.pmf(k=5, mu=3)
#         self.struggle_memory = np.random.randint(2, size=[memory_size, 3])
#
#         # 4 columns for:
#         # struggle resolution, opponent_categorization, and opponent_sex
#
#         self.rhp = np.random.normal()
#
#         self.total_payoff = 0
#         self.avg_payoff = 0
#
#         self.last_payoff = None
#         self.last_action = None
#         self.last_enemy_action = None
#
#     def __repr__(self):
#         return "Agent(%d)" % self.unique_id
#
#     def match(self, **kwargs):
#         return all(getattr(self, key) == val for (key, val) in kwargs.items())
#
#     def calc_avg_payoff_per_agent(self):
#         if self.age != 0:
#             self.avg_payoff = self.total_payoff / self.age
#         else:
#             pass
#
#     def imitate_categorization_if_better(self, opponent_list):  # best experienced payoff protocol
#         opponent = random.choice(opponent_list)
#         if opponent.avg_payoff > self.avg_payoff:
#             self.categorization = opponent.categorization
#         else:
#             pass
#
#     def imitate_categorization_proportional(self, opponent_list, m=0.5):  # best experienced payoff protocol
#         if random.random() < m:
#             opponent = random.choice(opponent_list)
#             if opponent.avg_payoff > self.avg_payoff:
#                 self.categorization = opponent.categorization
#             else:
#                 pass
#
#     def imitative_logit_categorization(self, opponent_list, m=0.1):  # best experienced payoff protocol
#
#         if random.random() < m:
#             opponent = random.choice(opponent_list)
#             if opponent.avg_payoff == self.avg_payoff == 0:
#                 pass
#             else:
#
#                 copy_probability = np.exp(m*opponent.avg_payoff) / \
#                                    (np.exp(m*self.avg_payoff) + np.exp(m*opponent.avg_payoff))
#
#                 if random.random() < copy_probability:
#                     self.categorization = opponent.categorization
#                 else:
#                     pass
#
#     def update(self, opponent, match_result):
#         # Checked and running correctly.
#         self.struggle_memory = np.delete(self.struggle_memory, 0, axis=0)
#         row_to_be_added = np.array([[match_result, opponent.categorization, opponent.sex]])
#         self.struggle_memory = np.append(self.struggle_memory, row_to_be_added, axis=0)
#         pass
#
#
# def categorize(categorizer, opponent):
#
#     mask = (categorizer.struggle_memory[:, 2] == opponent.sex)
#
#     if len(categorizer.struggle_memory[mask, :][:, 0]) > 0:
#         return categorizer.struggle_memory[mask, :][:, 0]
#     else:
#         return random.random()
#
#
# class Game:
#
#     def __init__(self, player1, player2, v=10, c=20, zeta=25, noise=0.0):
#
#         self.p1 = player1
#         self.p2 = player2
#         self.v = v
#         self.c = c
#         self.zeta = zeta
#         self.noise = noise
#
#         # print(1 / (1 + np.exp(-(self.p1.rhp - self.p2.rhp) * 5)))
#         self.rhp1 = 1 / (1 + np.exp(-(self.p1.rhp - self.p2.rhp) * zeta))
#         self.rhp2 = 1 - self.rhp1
#
#         if self.p1.categorization == 0:
#             self.erhp1 = np.average(self.p1.struggle_memory[:, 0])
#
#         elif self.p1.categorization == 1:
#             self.erhp1 = np.average(categorize(self.p1, self.p2))
#
#         if self.p2.categorization == 0:
#             self.erhp2 = np.average(self.p2.struggle_memory[:, 0])
#
#         elif self.p2.categorization == 1:
#             self.erhp2 = np.average(categorize(self.p2, self.p1))
#
#         if self.erhp1 == 0:
#             self.erhp1 = 0.01
#
#         if self.erhp2 == 0:
#             self.erhp2 = 0.01
#
#     def play(self):
#
#         if random.random() > self.noise:
#
#             if self.c/self.v < (1 - self.erhp1) / self.erhp1:
#                 strat1 = "hawk"
#
#             else:
#                 if self.erhp1 > 0.5:
#                     strat1 = "hawk"
#                 else:
#                     strat1 = "dove"
#
#         else:
#             strat1 = "hawk"
#
#         if random.random() > self.noise:
#
#             if self.c / self.v < (1 - self.erhp2) / self.erhp2:
#                 strat2 = "hawk"
#
#             else:
#                 if self.erhp2 > 0.5:
#                     strat2 = "hawk"
#                 else:
#                     strat2 = "dove"
#
#         else:
#             strat2 = "hawk"
#
#         if strat1 == strat2 == "hawk":
#             if random.random() < self.rhp1:
#                 self.p1.last_payoff = self.v
#                 self.p2.last_payoff = self.c
#                 self.p1.last_action = "hawk"
#                 self.p2.last_action = "hawk"
#                 self.p1.update(self.p2, 1)
#                 self.p2.update(self.p1, 0)
#             else:
#                 self.p1.last_payoff = self.c
#                 self.p2.last_payoff = self.v
#                 self.p1.last_action = "hawk"
#                 self.p2.last_action = "hawk"
#                 self.p1.update(self.p2, 0)
#                 self.p2.update(self.p1, 1)
#
#
#         elif strat1 == strat2 == "dove":
#             self.p1.last_payoff = self.v/2
#             self.p1.last_action = "dove"
#             self.p2.last_payoff = self.v/2
#             self.p2.last_action = "dove"
#         elif strat1 == "hawk" and strat2 == "dove":
#             self.p1.last_payoff = self.v
#             self.p1.last_action = "hawk"
#             self.p2.last_payoff = self.c
#             self.p2.last_action = "dove"
#         else:
#             self.p2.last_payoff = self.v
#             self.p2.last_action = "hawk"
#             self.p1.last_payoff = self.c
#             self.p1.last_action = "dove"
#         self.p1.total_payoff += self.p1.last_payoff
#         self.p2.total_payoff += self.p2.last_payoff
#
#         self.p1.last_enemy_action = self.p2.last_action
#         self.p2.last_enemy_action = self.p1.last_action
#
#
# class Environment:
#
#     def __init__(self, turn_amount, run_amount, pop_size, turn_over_prob=0.01,
#                  reward=10, cost=20, reliability=1, noise=0.01, memory_size=20):
#         self.total_turn = turn_amount
#         self.total_run = run_amount
#         self.pop_size = pop_size
#         self.current_turn = 0
#         self.current_run = 0
#         self.reward = reward
#         self.cost = cost
#         self.reliability = reliability
#         self.noise = noise
#         self.turn_over_prob = turn_over_prob  # This is essentially mutation rate.
#         self.memory_size = memory_size
#
#         self.__agents_list = []
#
#         self.number_male_agents = 0
#         self.number_female_agents = 0
#         self.number_sexist_agents = 0
#         self.number_regular_agents = 0
#
#         self.total_avg_payoff = 0
#         self.avg_male_payoff = 0
#         self.avg_female_payoff = 0
#         self.avg_regular_payoff = 0
#         self.avg_sexist_payoff = 0
#
#         self.run_df = pd.DataFrame()
#
#     def add_new_agent(self):
#
#         # 10 makes it realistic. Lower means 1/2, higher means 1.
#         if random.random() > np.exp(self.avg_male_payoff*10) / (np.exp(self.avg_male_payoff*10) +
#                                                              np.exp(self.avg_female_payoff*10)):
#             sex = 1
#         else:
#             sex = 0
#
#         categorization = np.random.randint(2)
#         self.__agents_list.append(Agent(self.__agents_list[-1].unique_id+1, categorization, sex,
#                                         memory_size=self.memory_size))
#
#     def add_init_agent(self, n):
#         self.__agents_list.append(Agent(n, n % 2, 0 if n < self.pop_size / 2 else 1,
#                                         memory_size=self.memory_size))
#
#     def find_agent(self, **kwargs):
#         return next(self.__iter_agent(**kwargs))
#
#     def all_agents(self, **kwargs):
#         return list(self.__iter_agent(**kwargs))
#
#     def __iter_agent(self, **kwargs):
#         return (agent for agent in self.__agents_list if agent.match(**kwargs))
#
#     def avg_payoffs(self, agent_list, **kwargs):
#         self.total_avg_payoff = 0
#         for agent in agent_list:
#             agent.calc_avg_payoff_per_agent()
#             self.total_avg_payoff += agent.avg_payoff
#         if len(agent_list) > 0:
#             list_avg_payoff = self.total_avg_payoff / len(agent_list)
#         else:
#             list_avg_payoff = 0
#         return list_avg_payoff
#
#
#     def avg_fighting(self, agent_list, **kwargs):
#         self.total_avg_pressure = 0
#         self.total_avg_aggro = 0
#         for agent in agent_list:
#             if agent.last_enemy_action == "hawk":
#                 self.total_avg_pressure += 1
#             if agent.last_action == "hawk":
#                 self.total_avg_aggro += 1
#
#         if len(agent_list) > 0:
#             list_avg_pressure = self.total_avg_pressure / len(agent_list)
#             list_avg_aggro = self.total_avg_aggro / len(agent_list)
#         else:
#             list_avg_pressure = 0
#             list_avg_aggro = 0
#
#         return [list_avg_pressure, list_avg_aggro]
#
#     def calc_avg_payoffs(self):
#
#         self.male_agents = self.all_agents(sex=0)
#         self.female_agents = self.all_agents(sex=1)
#         self.sexist_agents = self.all_agents(categorization=1)
#         self.regular_agents = self.all_agents(categorization=0)
#
#         self.number_male_agents = len(self.male_agents)
#         self.number_female_agents = len(self.female_agents)
#         self.number_sexist_agents = len(self.sexist_agents)
#         self.number_regular_agents = len(self.regular_agents)
#
#         self.avg_male_payoff = self.avg_payoffs(self.male_agents)
#         self.avg_female_payoff = self.avg_payoffs(self.female_agents)
#         self.avg_regular_payoff = self.avg_payoffs(self.regular_agents)
#         self.avg_sexist_payoff = self.avg_payoffs(self.sexist_agents)
#
#     def calc_distribution(self):
#
#         rf = len(self.all_agents(categorization=0, sex=1))
#         rm = len(self.all_agents(categorization=0, sex=0))
#         sf = len(self.all_agents(categorization=1, sex=1))
#         sm = len(self.all_agents(categorization=1, sex=0))
#
#         distribution = [rf, rm, sf, sm]
#         return distribution
#
#     def shuffle_agents(self):
#         shuffled_list = random.sample(self.__agents_list, len(self.__agents_list))
#         return shuffled_list
#
#     def match_robin(self, agents_list):
#         for n in range(int(len(agents_list)/2)):
#             p1 = agents_list[n]
#             p2 = agents_list[n+int(len(agents_list)/2)]
#             new_game = Game(p1, p2, v=self.reward, c=self.cost, zeta=self.reliability, noise=self.noise)
#             new_game.play()
#
#     def agents_update_logit(self):
#         for n in self.__agents_list:
#             n.imitative_logit_categorization(self.__agents_list)
#         pass
#
#     def turn_over_agents(self, turn_over_prob=0.1):
#         agents_fired = []
#         for agent in self.__agents_list:
#             if random.random() < turn_over_prob:
#                 agents_fired.append(agent)
#         self.__agents_list = [agent for agent in self.__agents_list if agent not in agents_fired]
#         for agent in range(len(agents_fired)):
#             self.add_new_agent()
#
#     def one_turn(self):
#
#         shuffled_list = self.shuffle_agents()
#         self.match_robin(shuffled_list)
#         self.agents_update_logit()
#
#         self.calc_avg_payoffs()
#         self.turn_over_agents(self.turn_over_prob)
#         female_press_aggro = self.avg_fighting(self.all_agents(sex=1))
#         male_press_aggro = self.avg_fighting(self.all_agents(sex=0))
#
#         sex_aggro_difference = male_press_aggro[1] - female_press_aggro[1]
#         sex_pressure_difference = male_press_aggro[0] - female_press_aggro[0]
#
#
#         distribution = self.calc_distribution()
#         list_to_df = [[self.current_run], [self.current_turn], distribution, female_press_aggro, male_press_aggro,
#                       [sex_aggro_difference], [sex_pressure_difference]]
#         self.run_df.loc[len(self.run_df)] = [item for sublist in list_to_df for item in sublist]
#         for agent in self.__agents_list:
#             agent.age += 1
#         self.current_turn += 1
#
#     def one_run(self):
#
#         self.current_turn = 0
#         self.__agents_list = []
#
#         self.number_male_agents = 0
#         self.number_female_agents = 0
#         self.number_sexist_agents = 0
#         self.number_regular_agents = 0
#
#         self.total_avg_payoff = 0
#         self.avg_male_payoff = 0
#         self.avg_female_payoff = 0
#         self.avg_regular_payoff = 0
#         self.avg_sexist_payoff = 0
#
#         self.run_df = pd.DataFrame(columns=["Run", "Turn", 'rf', 'rm', 'sf', 'sm', "female_pressure", "female_aggro",
#                                             "male_pressure", "male_aggro", "sex_aggro_diff", "sex_press_diff"])
#
#         for n in range(self.pop_size):
#             self.add_init_agent(n)
#
#         for i in range(self.total_turn):
#             self.one_turn()
#
#         print(self.run_df)
#
#         return self.run_df
#
#     def full_sim(self):
#
#         self.current_run = 0
#         self.all_run_dataframes = []
#         for i in range(self.total_run):
#
#             new_run_df = self.one_run()
#             self.all_run_dataframes.append(new_run_df)
#             self.current_run += 1
#         pass
#
#     def plot_average_categories(self):
#
#         averages_df = pd.concat(self.all_run_dataframes).groupby(level=0).mean()
#         # std = self.all_run_dataframes.groupby('category')['number'].std()
#         regular_columns = ['rf', 'rm']
#         sexist_columns = ['sf', 'sm']
#
#         averages_df['Regular_Averages'] = averages_df[regular_columns].sum(axis=1)
#         averages_df['Sexist_Averages'] = averages_df[sexist_columns].sum(axis=1)
#
#         plt.plot(averages_df['Regular_Averages'], label="regular")
#         plt.plot(averages_df['Sexist_Averages'], label="sexist")
#         plt.legend()
#         plt.show()
#
#     def plot_gender_segregation(self):
#         averages_df = pd.concat(self.all_run_dataframes).groupby(level=0).mean()
#
#         # std = self.all_run_dataframes.groupby('category')['number'].std()
#         female_columns = ['rf', 'sf']
#         male_columns = ['rm', 'sm']
#
#         averages_df['Female_Pop'] = averages_df[female_columns].sum(axis=1)
#         averages_df['Male_Pop'] = averages_df[male_columns].sum(axis=1)
#
#         plt.plot(averages_df['Female_Pop'], label="females")
#         plt.plot(averages_df['Male_Pop'], label="males")
#         plt.legend()
#         plt.show()
#
#         plt.plot(averages_df['sex_aggro_diff'], label="Aggressiveness Difference")
#         plt.legend()
#         plt.show()
#
#         plt.plot(averages_df['sex_press_diff'], label="Pressure Difference")
#         plt.legend()
#         plt.show()
#
#
# def sigmoid(sep=200, zeta=1):
#
#     x = np.linspace(-1, 1, sep)
#
#     z = 1 / (1 + np.exp(-x * zeta))
#
#     plt.plot(x, z)
#     plt.xlabel("Power Difference")
#     plt.ylabel("Probability of winning when a is 0.1")
#
#     plt.show()
#
# # sigmoid(200)
#
# #AGENT NUMBERS CHANGE EVERYTHING!
#
# #RELIABILITY 50 YETIYOR, MEMORY'yi 200'den 100'e DUSURDUM
# #TURU 2000'e CIKAR!!!!!?!?!??!?!?!/
# #100 RUN GALIBA YETMIYOOOOOOOOOR!?!?!?!?
# t0 = time.time()
# low_cust_low_reliability_high_memory = Environment(turn_amount=5000, run_amount=50, pop_size=240, turn_over_prob=0.001,
#                                                    reward=10, cost=20, reliability=5, noise=0.01, memory_size=10)
# low_cust_low_reliability_high_memory.full_sim()
# t1 = time.time()
# total = t1-t0
# print(total/60)
# low_cust_low_reliability_high_memory.plot_average_categories()
# low_cust_low_reliability_high_memory.plot_gender_segregation()
